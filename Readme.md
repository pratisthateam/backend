# StackBy.com

### Requirement
- Node.js 8.10
- Serverless ^1.27.3

### Installation
-> Install the dependencies.

```sh
$ npm install
```
or
```sh
$ yarn install (recommended)
```

### Configuration
1. Create ```config.json``` in root directory.
2. Add following ```key: values``` to the file.
```
{
  "dev": {
    "accountId": "<AWS_ACCOUNT_ID>",
    "memorySize": "<AWS_LAMBDA_MEMORY_SIZE>",
    "region": "<AWS_COGNITO_USERPOOL_REGION>",
    "MONGO_URL": "<MLAB_COLLECTION_URL>",
    "userPoolId": "<AWS_COGNITO_USERPOOL_ID>"
  },
  "prod": {
    "accountId": "<AWS_ACCOUNT_ID>",
    "memorySize": "<AWS_LAMBDA_MEMORY_SIZE>",
    "region": "<AWS_COGNITO_USERPOOL_REGION>",
    "MONGO_URL": "<MLAB_COLLECTION_URL>",
    "userPoolId": "<AWS_COGNITO_USERPOOL_ID>"
  }
}
```

### Deployment

-> Create ```deploy.dev ``` file in root directory with below values for deploying to development environment 

```sh
export AWS_ACCESS_KEY_ID=<AWS_ACCESS_KEY>
export AWS_SECRET_ACCESS_KEY=<AWS_SECRET_ACCESS_KEY>
export REGION=<AWS_REGION>

serverless deploy --stage dev
```

-> Create ```deploy.prod ``` file in root directory with below values for deploying to production environment 

```sh
export AWS_ACCESS_KEY_ID=<AWS_ACCESS_KEY>
export AWS_SECRET_ACCESS_KEY=<AWS_SECRET_ACCESS_KEY>
export REGION=<AWS_REGION>

serverless deploy --stage prod
```

> Run ```./deploy.dev``` or ```./deploy.prod``` in terminal from root directory.

### APIS
