const util = require('util');
const AWS = require('aws-sdk');
const csv = require('csvtojson');
const { openDb } = require('./db');

let db;
const response = {
    statusCode: 200,
    headers: {
        'Access-Control-Allow-Origin': '*'
    },
    body: ''
};

module.exports.getAllFiles = async (event, context, callback) => {
    try {
        const S3 = new AWS.S3({ region: 'us-east-1' });
        context.callbackWaitsForEmptyEventLoop = false;
        // const username = event.requestContext.authorizer.claims['cognito:username'];
        db = await util.promisify(openDb)();
        const model = db.model('Uploads');
        let uploads = await model.find({ username: 'brijesh' });
        uploads = await Promise.all(
            uploads.map(async upload => {
                try {
                    upload = upload.toJSON();
                    const data = await S3.getObject({
                        Bucket: 'bar-chart-frontendcc4caaae1f7f4215986751878de4d37c-dev',
                        Key: `public/${upload.file_path}`
                    }).promise();
                    upload.data = await csv().fromString(data.Body.toString());
                    return upload;
                } catch(err) {
                    console.log(err);
                    return null;
                }
            })
        )
        response.statusCode = 200;
        response.body = JSON.stringify(uploads.filter(x => x));
        callback(null, response);
    } catch (err) {
        callback(err);
    }
}

module.exports.uploadFile = async (event, context, callback) => {
    try {
        context.callbackWaitsForEmptyEventLoop = false;
        const username = event.requestContext.authorizer.claims['cognito:username'];
        console.log(event.body);
        body = JSON.parse(event.body);
        db = await util.promisify(openDb)();
        const model = db.model('Uploads');
        const upload = await model.create({ username, ...body });
        response.statusCode = 200;
        response.body = JSON.stringify(upload);
        callback(null, response);
    } catch (err) {
        callback(err);
    }
}
