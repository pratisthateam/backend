const mongoose = require('mongoose');
const UploadsModel = require('../models/Uploads');

const Schema = mongoose.Schema;

const versionKey = {
  versionKey: false,
  timestamps: true
};

mongoose.Promise = global.Promise;
let cachedDB = null;


const Uploads = new Schema(UploadsModel, versionKey);
mongoose.model('Uploads', Uploads);

const db = {
  openDb: async cb => {
    const url = process.env.MONGO_URL;
    const options = {
      keepAlive: 15000,
      connectTimeoutMS: 20000,
      useNewUrlParser: true,
      useCreateIndex: true,
      useFindAndModify: false
    };

    if (cachedDB && cachedDB.connections[0].readyState) {
      console.log('Using cached connection');
      return cb(null, cachedDB);
    }

    try {
      cachedDB = await mongoose.connect(
        url,
        options
      );
      console.log('Successfull Mongo connection');
      return cb(null, cachedDB);
    } catch (e) {
      console.log('Mongo connection failed', e);
      return cb(e);
    }
  }
};
module.exports = db;