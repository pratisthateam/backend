module.exports = {
    username: { type: String, required: true },
    file_path: { type: String, required: true, },
    x_axis: { type: String, required: true },
    y_axis: { type: String, required: true }
};